import Test.Hspec
import Test.Hspec.QuickCheck
import Test.QuickCheck

main :: IO ()
main = hspec $ do
    describe "read" $ do
        it "is inverse to show" $ property
            $ \x -> (read . show) x `shouldBe` (x :: Int)
