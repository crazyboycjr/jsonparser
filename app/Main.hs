module Main where

import Data.Functor
import Lib (parseJson)
import System.Environment (getArgs)
import Text.Parsec (eof, errorPos)
import Text.Parsec.String (parseFromFile)

main :: IO ()
main = do
    file <- getArgs <&> head . (++ ["example.json"])
    res  <- parseFromFile (parseJson <* eof) file
    case res of
        Left err -> do
            print err
            print $ errorPos err
        Right json -> print json
