module Lib
    ( parseJson
    ) where

import Text.Parsec ((<?>), (<|>), between, choice, many, noneOf, option, sepBy, try)
import Text.Parsec.Char (anyChar, char, digit, spaces, string)
import Text.Parsec.Combinator (many1)
import Text.Parsec.String (Parser)

import Control.Applicative (liftA2)
import Data.Functor ((<&>))
import qualified Data.Map.Ordered as M

-- | JavaScript Numbers are Always 64-bit Floating Point.
-- Currently, scientific notation is not supported.
data JsonValue = JsonNull
               | JsonBool Bool
               | JsonString String
               | JsonNumber Double
               | JsonObject (M.OMap String JsonValue)
               | JsonArray [JsonValue]
               deriving (Show)

symbol :: String -> Parser String
symbol s = do
    spaces
    sym <- string s
    spaces
    return sym

parseNull :: Parser JsonValue
parseNull = string "null" >> return JsonNull

parseBool :: Parser JsonValue
parseBool = (string "true" <|> string "false") <&> f
  where
    f "true"  = JsonBool True
    f "false" = JsonBool False
    f x       = error $ "unexpected: " ++ x

jsonStringEscape :: Parser Char
jsonStringEscape = char '\\'

parseString :: Parser JsonValue
parseString = do
    char '"'
    str <- many $ try (jsonStringEscape >> anyChar) <|> noneOf ['"']
    char '"'
    return $ JsonString str

parseNumber :: Parser JsonValue
parseNumber = do
    sign           <- option "" $ string "-"
    intPart        <- many1 digit
    fractionalPart <- option "" $ liftA2 (:) (char '.') (many digit <&> (++ "0"))
    return $ JsonNumber (read $ sign ++ intPart ++ fractionalPart)

parseObject :: Parser JsonValue
parseObject = do
    symbol "{"
    kvs <- parseKeyValue `sepBy` try (symbol ",")
    symbol "}"
    return $ JsonObject (M.fromList kvs)
  where
    parseKeyValue = do
        JsonString key <- parseString
        symbol ":"
        value <- parseJson
        return (key, value)

parseArray :: Parser JsonValue
parseArray = between (symbol "[") (symbol "]") $ do
    objs <- parseJson `sepBy` try (symbol ",")
    return $ JsonArray objs

parseJson :: Parser JsonValue
parseJson = between spaces spaces
    $ choice [parseNull, parseBool, parseString, parseNumber, parseObject, parseArray]
